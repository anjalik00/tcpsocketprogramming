# import socket module
import sys
from socket import *

serverSocket = socket(AF_INET, SOCK_STREAM)
# this creates a socket

port = 8000 # port on which we want socket to listen on
host = "" # server IP gets added.
serverSocket.bind((host, port))
serverSocket.listen(1) # number of connections it will accept

while True:
    # Establish the connection
    print('\r\nReady to serve...')
    connectionSocket, address = serverSocket.accept()

    print("Client connection address: " + str(address))

    try:
        message = connectionSocket.recv(1024).decode()
        if not message:
            connectionSocket.close()
            continue
        print(str(message))
        filename = message.split()[1]
        print("Filename requested is:" + str(filename))
        f = open(filename[1:])  # opens file and reads the content
        outputdata = f.read()

        # Send one HTTP header line into socket
        connectionSocket.send("\nHTTP/1.1 200 OK\n".encode())

        # Send the content of the requested file to the client
        for i in range(0, len(outputdata)):
            connectionSocket.send(outputdata[i].encode())  # this needs to be a loop because file wont consist of just one element - it will consist of multiple elementswhich you need to loop over, which you dont know length of and each othe element needs to be encoded
        connectionSocket.send("\r\n".encode())  # don't know why this is done - what is this?
        print('File sent!\r\n')
        connectionSocket.close()
    except IOError:

        # Send response message for file not found
        connectionSocket.send("\nHTTP/1.1 404 Not Found\n".encode())
        connectionSocket.send("\n<html><head></head><body><h1>404 Not Found</h1></body></html>\n".encode())

        # Close client socket
        connectionSocket.close()

serverSocket.close()
sys.exit()  # Terminate the program after sending the corresponding data
